CC = gcc
CXX = g++-4.8
CXXFLAGS = -pipe -Wall -Wno-deprecated-declarations -pthread -m64 -I$(ROOTSYS)/include -DESAFARCH=x86_64 -g -std=c++1y -Wall -Wextra -Wshadow -Wundef -Wfloat-equal -Wmain -W -D_REENTRANT -fPIC
CFLAGS = -std=gnu99 -Walln -pedantic -Wchar-subscripts -g
LDFLAGS=-L$(ROOTSYS)/lib -lGui -lCore -lCint -lRIO -lNet -lHist -lGraf -lGraf3d -lGpad -lTree -lRint -lPostscript -lMatrix -lPhysics -lMathCore -pthread -lm -ldl -rdynamic -m64 -lGeom -lGeomPainter -lMinuit -lGed -lThread -lSpectrum 
SOURCE = TrigVisMain.cc
EXECUTABLE = a.out
SRC = src
TMP = tmp
SOURCES := $(wildcard $(SRC)/*.cc)
OBJECTS := $(patsubst $(SRC)/%.cc, $(TMP)/%.o, $(SOURCES))
# ZIP=amon2.zip

.PHONY: all clean zip clean-zip

all: $(OBJECTS) $(SOURCE) $(EXECUTABLE) compareConversion.exe

clean:
	@rm -f $(EXECUTABLE) $(OBJECTS) compareConversion.exe

$(EXECUTABLE): $(SOURCES)
	$(CXX) $(CXXFLAGS) $(SOURCE) $(LDFLAGS) -o $@ $(OBJECTS) $(LDFLAGS)

$(TMP)/%.o: $(SRC)/%.cc
	$(CXX) $(CXXFLAGS) -Iinclude $(LDFLAGS) -c $< -o $@

compareConversion.exe: CompareConversion.cc
	$(CXX) $(CXXFLAGS) CompareConversion.cc $(LDFLAGS) -o $@