import os
import sys
import re
import subprocess
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-o','--output-dir',  help='path to output dir', required=True, type=str)
parser.add_argument('-d','--data-dir',  help='path to data dir', required=True, type=str)
parser.add_argument('-t','--trig-vis-kenji',  help='path to trigVisKenji file', required=True, type=str)
args, unknown = parser.parse_known_args()

arguments = ''
for argument in unknown:
	arguments = arguments + ' ' + argument

for root, subFolders, files in os.walk(args.output_dir):
	dataRoot = root.replace(args.output_dir, args.data_dir,1)
	if not os.path.exists(dataRoot):
		os.makedirs(dataRoot)
	for filename in files:
		if re.match('[\D\d]*ACQUISITION[\D\d]*.root$', filename):
			outDir = ' --outdir ' + dataRoot + ' ' 
			filePath = os.path.join(root, filename)
			command = args.trig_vis_kenji + arguments + outDir + filePath
			subprocess.call(command, shell=True)
