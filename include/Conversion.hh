/*
 * Conversion.hh
 *
 *  Created on: Feb 18, 2018
 *      Author: Kristián Goliaš
 */

#ifndef TRIGVISKENJIV3_SRC_CONVERSION_HH_
#define TRIGVISKENJIV3_SRC_CONVERSION_HH_

#include <iostream>

#include "TString.h"
#include "TFile.h"
#include "TTree.h"

class Conversion {
public:
    Int_t convertFile3(TString simuout, TString outsuffix);
    Conversion();
    virtual ~Conversion();
};

#endif /* TRIGVISKENJIV3_SRC_CONVERSION_HH_ */
