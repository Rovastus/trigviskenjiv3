/*
 * Trigger.hh
 *
 *  Created on: Feb 25, 2018
 *      Author: Kristián Goliaš
 */

#ifndef TRIGVISKENJIV3_SRC_TRIGGER_HH_
#define TRIGVISKENJIV3_SRC_TRIGGER_HH_

#include "Rtypes.h"
#include "TString.h"
#include "TH2I.h"
#include "TH1I.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TColor.h"
#include "../src/Iterator.cc"

#include "Option.hh"

class Trigger {
private:
    std::vector<Int_t> numberOfFrameForHistogram; //Visualization
    std::vector<Int_t> triggerRateForHistogram; //Visualization
    std::vector<Float_t> maxForPMTvalues[18][2]; //Visualization
    Float_t thresholdsForPMT[18][2];
    Float_t thresholdsForPMTGlobalGTU[18][2];
    Bool_t validTresholdsForPMT[18][2];
    UInt_t validMaxForPMT[18][2]; //Visualization
    UInt_t invalidMaxForPMT[18][2]; //Visualization
    Float_t minThresholdsForPMT[18][2];
    Float_t maxThresholdsForPMT[18][2];
    Int_t chosenThresholds[2][18][2];
    Float_t triggerThresholds[100][5];
    Float_t inverseMeansFrame[2304];
    Double_t gtuCounter;
    Int_t maskPlot[144][16];
    Option *option;
    Double_t getXUpValueForHistogram(Int_t maxValue, Int_t numberOfBean);
    std::string getTime();
    void setChosenTresholds();
public:
    void createHistogramAndGraphForMaxForPMT();
    void createHistogramForThresholds();
    void createHistogramForNumberOfFrame();
    void createHistogramForTriggerRate();
    void readThresholdsForFirstPacket(Float_t ThresholdLoweringFactor,
                                    Int_t packet_size,
                                    fileIterator<Int_t> *iterate);
    void triggeralorithm_fenu_rootout(TString outputprefix,
                                     Float_t ThresholdLoweringFactor,
                                     Int_t packet_size,
                                     fileIterator<Int_t> *iterate);
    void setChosenTresholds(const Int_t row, const Int_t col, const Float_t &triggerBin);
    Int_t getCountValidPMT();
    Trigger(Option *option);
    virtual ~Trigger();
};

#endif /* TRIGVISKENJIV3_SRC_TRIGGER_HH_ */
