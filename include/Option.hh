/*
 * Option.hh
 *
 *  Created on: Feb 4, 2018
 *      Author: Kristián Goliaš
 */

#ifndef TRIGVISKENJIV3_SRC_OPTION_HH_
#define TRIGVISKENJIV3_SRC_OPTION_HH_

#include <getopt.h>
#include <sys/stat.h>

#include <iostream>
#include <stdexcept>
#include <string>
#include <cstdlib>

#include "Rtypes.h"
#include "TString.h"

class Option {
private:
    std::vector<std::string> inputFiles;
    TString dataDir;
    TString thresholdsFilePathname;
    TString minSignalsFilePathname;
    TString maxSignalsFilePathname;
    TString maskFilePathname;
    TString inverseMeansFramePathname;
    Bool_t help;
    Bool_t visualisation;
    Bool_t convertYes;
    Bool_t dotrigger;
    Bool_t simplePrefix;
    Bool_t ecAscInput;
    Bool_t readThresholdsForFirstPacket;
    Bool_t useMinAndMaxSignals;
    Bool_t useThresholdForPDM;
    Bool_t triggerVisual;
    Bool_t inverseMeansFrame;
    Int_t packetSize;
    Float_t ave_bg_factor;
    Int_t minNumValidPMT;
public:
    Option(Int_t argc, char **argv);
    virtual ~Option();
    Bool_t isConvertYes() const;
    const TString& getDataDir() const;
    Bool_t isDotrigger() const;
    Bool_t isEcAscInput() const;
    Int_t getHelp() const;
    const std::vector<std::string>& getInputFiles() const;
    const TString& getMaskFilePathname() const;
    const TString& getMaxSignalsFilePathname() const;
    const TString& getMinSignalsFilePathname() const;
    Int_t getPacketSize() const;
    Bool_t isSimplePrefix() const;
    const TString& getThresholdsFilePathname() const;
    Bool_t isVisualisation() const;
    Bool_t isHelp() const;
    Bool_t isReadThresholdsForFirstPacket() const;
    Bool_t isUseMinAndMaxSignals() const;
    Float_t getAveBgFactor() const;
    Bool_t isUseThresholdForPdm() const;
    Int_t getMinNumValidPmt() const;
    Bool_t isTriggerVisual() const;
    Bool_t isInverseMeansFrame() const;
    const TString& getInverseMeansFramePathname() const;
};

#endif /* TRIGVISKENJIV3_SRC_OPTION_HH_ */
