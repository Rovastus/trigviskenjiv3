#include <string>
#include <stdio.h>
#include <iostream>

int main(int argc, char** argv) {
    if(argc < 3) {
        std::cout << "Usage: ./CompareConversion [file1] [file2]" << std::endl;
    }

    std::string firstArgument = argv[1];
    std::string secondArgument = argv[2];
    std::string firstFile = "gzip -dc " + firstArgument;
    std::string secondFile = "gzip -dc " + secondArgument;

    FILE* file1;
    FILE* file2;

    file1 = popen(firstFile.c_str(), "r");
    file2 = popen(secondFile.c_str(), "r");

    int number1 = 0;
    int number2 = 0;

    while (fscanf(file1, "%d", &number1) != EOF) {
        fscanf(file2, "%d", &number2);
        if(number1 != number2) {
            std::cout << "PROBLEM " << number1 << " !== " << number2 << std::endl;
            return -1;
        }
    }

    if(fscanf(file2, "%d", &number2) != EOF) {
        std::cout << secondArgument << " nieje na EOF, zatiaľ čo " << firstArgument << " je na EOF." << std::endl;
    }else {
        std::cout << secondArgument << " a " << firstArgument << " sú rovnaké." << std::endl;
    }

    return 0;
}
