#include <execinfo.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include "TString.h"
#include "TH2I.h"
#include "TH2D.h"
#include "TH3I.h"
#include "TGraph.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TLatex.h"
#include "TFile.h"
#include "TROOT.h"
#include "TColor.h"
#include "TSystem.h"
#include "TTree.h"
#include <iostream>
#include <cstring>
#include <string>
#include <vector>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdexcept>
#include <chrono>  // for high_resolution_clock

#include "include/Palette.hh"
#include "include/Option.hh"
#include "include/Conversion.hh"
#include "include/Visualization.hh"
#include "include/Trigger.hh"

#define BT_BUF_SIZE 100

void printHelp() {
    fprintf(stderr,"./a.out [option] allxxx*1.root allxx*2.root \n");
    fprintf(stderr,"--help, -h : show help \n");
    fprintf(stderr,"--dir,--outdir,--out, -o : output_directory (where output root file and selected images will store) \n");
    fprintf(stderr,"--bgfactor,-b : trigger_threshold_reduction_factor \n");
    fprintf(stderr,"--skip-conver,-C : no conversion to ascii from root \n");
    fprintf(stderr,"--skip-trigger,-T : skip trigger \n");
    fprintf(stderr,"--skip-visual,-V : no visualisation \n");
    fprintf(stderr,"--thresholds,-t : L1 triggere thresholds file table \n");
    fprintf(stderr,"--min-thresholds,-A : min thresholds value per PMT (18x2 format) \n");
    fprintf(stderr,"--max-thresholds,-B : max thresholds value per PMT (18x2 format) \n");
    fprintf(stderr,"--mask,-m : L1 triggere thresholds file table \n");
    fprintf(stderr,"--simple-prefix, -s : File name is not checked for specific name format, filename prefixed by \"trn\" and without extension is the prefix, squeeze is not checked \n");
    fprintf(stderr,"--ec-asc-input, -e : EC ASC format (mario) is expected as the input, conversion is skipped \n");
    fprintf(stderr,"--packet-size, -p : size of the packed, by default 128 \n");
    fprintf(stderr,"--read-thresholds-for-first-packet, -R : read thresholds for first packet (2nd packet from file)\n");
    fprintf(stderr,"--use-min-and-max-thresholds, -U : read thresholds for first packet (2nd packet from file)\n");
    fprintf(stderr,"--use-threshold-for-pdm, -D : tresholds PDM-wise\n");
    fprintf(stderr,"--min-num-valid-pmt, -E : minimal number of valid PMT in GTU\n");
}

Int_t main(Int_t argc, char** argv) {
    gSystem->Load("libTree.so");

    try {
        Palette *palette = new Palette();
        palette->InvBPalette();
        Option *option = new Option(argc, argv);

        if(option->isHelp()) {
            printHelp();
            return -1;
        }

        std::vector<std::string> inputFiles = option->getInputFiles();
        Float_t aveBgFactor = option->getAveBgFactor();

        Visualization *visualization = new Visualization();
        Conversion *conversion = new Conversion();
        Trigger *trigger = new Trigger(option);

        for(std::vector<std::string>::iterator inputFileIt=inputFiles.begin(); inputFileIt!=inputFiles.end(); ++inputFileIt) {
            std::string fname = *inputFileIt;

            std::cout << "Processing file: "  << fname << std::endl;

            std::size_t lastSlashPos = fname.find_last_of("/\\");
            if(lastSlashPos == std::string::npos) {
                lastSlashPos = 0;
            } else if(lastSlashPos+1 >= fname.length()) {
                std::cout <<  "Missing file name in the path on input (\"" << fname << "\")" << std::endl;
                exit(7);
            }

            TString outputprefix;
            TString outputprefixShort;

            if(!option->isSimplePrefix()) {
                std::string::size_type aquisition = fname.find("ACQUISITION-", lastSlashPos);
                std::string::size_type squeeze = fname.find("sqz", lastSlashPos);

                if(squeeze != std::string::npos)
                    squeeze = 1;

                if(aquisition == std::string::npos || (!option->isEcAscInput() && fname.substr(fname.length()-5,5) != ".root")) {
                    std::cout << fname << " : not an ACQUISITION root file" << std::endl;
                    return -1;
                }

                if (squeeze == 1) {
                    aveBgFactor *= Float_t(option->getPacketSize())/25.0f;   // !!! WARNING changed from 128./24 to 128./25
                }

                outputprefix = option->getDataDir();
                if(outputprefix[outputprefix.Length()-1] != '/') {
                    outputprefix += "/";
                }
                outputprefix += "trn_" + fname.substr(aquisition+12,23);
                outputprefixShort = outputprefix;
                outputprefix += "_bgf_" + TString(Form("%.2f",aveBgFactor));
            }
            else {
                std::size_t lastDotPos = fname.find_last_of(".");
                std::string filenameNoExt;
                if(lastDotPos != std::string::npos && lastDotPos > lastSlashPos) {
                    filenameNoExt = fname.substr(lastSlashPos+1, lastDotPos-lastSlashPos-1);
                } else {
                    filenameNoExt = fname.substr(lastSlashPos+1);
                }
                outputprefixShort = option->getDataDir();
                if(outputprefixShort[outputprefixShort.Length()-1] != '/') {
                    outputprefixShort += "/";
                }
                outputprefixShort += "trn_" + TString(filenameNoExt.c_str());
                outputprefix = TString::Format("%s_bgf_%.2f", outputprefixShort.Data(), aveBgFactor);
            }

            std::cout << argc << " " << outputprefix << " " << outputprefixShort << " | " << fname <<  " { | | " << argv[1] << std::endl;

            struct stat dirInfo;
            if(stat( option->getDataDir(), &dirInfo ) != 0) {
                std::cerr << "cannot access " << option->getDataDir() << std::endl;
                exit(3);
            }
            else if(!(dirInfo.st_mode & S_IFDIR)) {
                std::cout << "data_dir " << option->getDataDir() << " is not directory" << std::endl;
                exit(4);
            }

            TString triggerInputName;

            if(option->isConvertYes() == true && !option->isEcAscInput()) {
                conversion->convertFile3(TString(fname),outputprefixShort);
                triggerInputName = TString::Format("%s_ec_asc.gz",outputprefixShort.Data());
            }
            else {
                std::cout << "Skipping conversion" << std::endl;
                triggerInputName = fname.c_str();
            }

            if(option->isDotrigger() == true) {
                try {
                    // read first packet from first file
                    Int_t file_position =  std::distance(inputFiles.begin(), inputFileIt);
                    if(file_position == 0 && option->isReadThresholdsForFirstPacket()) {
                        if(!option->isConvertYes()) {
                            TFile *inputFile = new TFile(TString(fname.c_str()));
                            fileIterator<Int_t> fileToIterate(inputFile, false);
                            trigger->readThresholdsForFirstPacket(aveBgFactor, option->getPacketSize(), &fileToIterate);
                        } else {
                            TString pfilename = "gzip -dc ";
                            pfilename += triggerInputName;
                            FILE* file = popen(pfilename, "r");
                            fileIterator<Int_t> fileToIterate(file);
                            trigger->readThresholdsForFirstPacket(aveBgFactor, option->getPacketSize(), &fileToIterate);
                        }
                    }

                    if(!option->isConvertYes()) {
                        TFile *inputFile = new TFile(TString(fname.c_str()));
                        fileIterator<Int_t> fileToIterate(inputFile, false);
                        trigger->triggeralorithm_fenu_rootout(outputprefix, aveBgFactor, option->getPacketSize(), &fileToIterate);
                    } else {
                        TString pfilename = "gzip -dc ";
                        pfilename += triggerInputName;
                        FILE* file = popen(pfilename, "r");
                        fileIterator<Int_t> fileToIterate(file);
                        trigger->triggeralorithm_fenu_rootout(outputprefix, aveBgFactor, option->getPacketSize(), &fileToIterate);
                    }
                } catch (std::logic_error & ex) {
                    std::cerr << "Exception: " << ex.what() << std::endl;
                    return -1;
                } catch (std::invalid_argument & ex) {
                    std::cerr << "Exception: " << ex.what() << std::endl;
                    return -1;
                }
            } else {
                std::cout << "Skipping triggering algorithm" << std::endl;
            }

            if(option->isVisualisation() == true && !option->isEcAscInput()) {
                TString trgrootfile = outputprefix + ".root";
                visualization->TrigVis7(TString(fname), trgrootfile, outputprefix);
            } else {
                std::cout << "Skipping visualization" << std::endl;
            }
        }

        if(option->isTriggerVisual()) {
            trigger->createHistogramAndGraphForMaxForPMT();

            if(option->isUseMinAndMaxSignals()) {
                trigger->createHistogramForNumberOfFrame();
                trigger->createHistogramForThresholds();
                trigger->createHistogramForTriggerRate();
            }
        }

        delete trigger;
        delete visualization;
        delete conversion;
        delete option;
        delete palette;
    } catch (std::invalid_argument & ex) {
        std::cerr << "Exception: " << ex.what() << std::endl;
        printHelp();
        return -1;
    }

	return 0;
}
