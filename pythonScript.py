import os
import sys
import re
import subprocess
import argparse

def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

parser = argparse.ArgumentParser()
parser.add_argument('-r','--rootdir',  help='path to dir', required=True, type=str)
parser.add_argument('-t','--trigVisKenji',  help='path to trigVisKenji file', required=True, type=str)
parser.add_argument('-a','--allFile', type=str2bool, nargs='?', const=False, help='use more than one file for trigvisKenji')
args, unknown = parser.parse_known_args()

arguments = ''
for argument in unknown:
	arguments = arguments + ' ' + argument

if args.allFile:
	filePaths = ''
	for root, subFolders, files in os.walk(args.rootdir):
		for filename in files:
			filePath = os.path.join(root, filename)
			if re.match('[\D\d]*ACQUISITION[\D\d]*.root$', filename):
				filePaths = filePaths + " " + filePath


	command = args.trigVisKenji + arguments + filePaths
	subprocess.call(command, shell=True)
else:
	for root, subFolders, files in os.walk(args.rootdir):
		for filename in files:
			filePath = os.path.join(root, filename)
			if re.match('[\D\d]*ACQUISITION[\D\d]*.root$', filename):
				command = args.trigVisKenji + arguments + ' ' + filePath
				subprocess.call(command, shell=True)
