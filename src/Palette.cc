/*
 * Palette.cc
 *
 *  Created on: Feb 4, 2018
 *      Author: Kristián Goliaš
 */

#include "Palette.hh"

Palette::Palette() {
    // TODO Auto-generated constructor stub

}

Palette::~Palette() {
    // TODO Auto-generated destructor stub
}

Int_t Palette::BPalette()
{
    static const Int_t kN = 100;
    static Int_t colors[kN];
    static Bool_t initialized = kFALSE;
    Double_t r[]     = {0,0,1,1,1};
    Double_t g[]     = {0,0,0,1,1};
    Double_t b[]     = {0,1,0,0,1};
    Double_t stop[]  = {0,0.25,0.5,0.75,1};

    if(!initialized) {
        Int_t index = TColor::CreateGradientColorTable(5, stop, r, g, b, kN);
        for(Int_t i=0; i<kN; i++) {
            colors[i] = index + i;
        } // i
        initialized = kTRUE;
    } else {
        gStyle->SetPalette(kN, colors);
    } // if

    return 0;
}

Int_t Palette::InvBPalette()
{
    static const Int_t kN = 100;
    static Int_t colors[kN];
    static Bool_t initialized = kFALSE;
    Double_t r[]     = {1,1,1,0,0};
    Double_t g[]     = {1,1,0,0,0};
    Double_t b[]     = {1,0,0,1,0};
    Double_t stop[]  = {0,0.25,0.5,0.75,1};

    if(!initialized) {
        Int_t index = TColor::CreateGradientColorTable(5, stop, r, g, b, kN);
        for (Int_t i=0; i<kN; i++) {
            colors[i] = index + i;
        } // i
        initialized = kTRUE;
    } else {
        gStyle->SetPalette(kN, colors);
    } // if

    return 0;
}
