/*
 * Option.cc
 *
 *  Created on: Feb 4, 2018
 *      Author: Kristián Goliaš
 */

#include "Option.hh"

Option::Option(Int_t argc, char **argv) {
    dataDir = "./";
    thresholdsFilePathname = "";
    minSignalsFilePathname = "";
    maxSignalsFilePathname = "";
    inverseMeansFramePathname = "";
    maskFilePathname = "./maskNOW.txt";
    help = false;
    visualisation = true;
    convertYes = true;
    dotrigger = true;
    simplePrefix = false;
    ecAscInput = false;
    readThresholdsForFirstPacket = false;
    useMinAndMaxSignals = false;
    useThresholdForPDM = false;
    triggerVisual = false;
    inverseMeansFrame = false;
    packetSize = 128;
    ave_bg_factor = 1;
    minNumValidPMT = -1;

    Int_t c;
    Int_t option_index = 0;
    static struct option long_options[] =
            { { "help", no_argument, 0, 'h' },
              { "dir", required_argument, 0, 'o' },
              { "outdir", required_argument, 0, 'o' },
              { "out", required_argument, 0, 'o' },
              { "skip-convert", required_argument, 0, 'C' },
              { "skip-trigger", required_argument, 0, 'T' },
              { "skip-visual", required_argument, 0, 'V' },
              { "simple-prefix", required_argument, 0, 's' },
              { "ec-asc-input", required_argument, 0, 'e' },
              { "thresholds", required_argument, 0, 't' },
              { "min-signals", required_argument, 0, 'A' },
              { "max-signals", required_argument, 0, 'B' },
              { "mask", required_argument, 0, 'm' },
              { "bgfactor", required_argument, 0, 'b' },
              { "packet-size", required_argument, 0, 'p' },
              { "read-thresholds-for-first-packet", required_argument, 0, 'R' },
              { "use-min-and-max-signals", required_argument, 0, 'U' },
              { "use-threshold-for-pdm", required_argument, 0, 'D' },
              { "min-num-valid-pmt", required_argument, 0, 'E'},
              { "trigger-visual", required_argument, 0, 'F'},
              { "inverse-means-frame", required_argument, 0, 'G'},
              { "use-inverse-means-frame", required_argument, 0, 'H'},
              { 0, 0, 0, 0 }
            };

    while((c = getopt_long(argc, argv, "ho:C:T:V:s:e:t:A:B:m:b:p:R:U:L:D:E:F:G:H:", long_options, &option_index)) != -1) {
        switch (c) {
            case 'h':
                help = true;
                break;
            case 'o':
                dataDir = TString(optarg);
                std::cout << "Output directory: " << dataDir << std::endl;
                break;
            case 'C':
                if(strcmp(optarg,"n")==0 || strcmp(optarg,"0")==0 || strcmp(optarg,"no")==0 || strcmp(optarg,"false")==0 || strcmp(optarg,"False")==0) {
                    convertYes = true;
                } else if(strcmp(optarg,"y")==0 || strcmp(optarg,"1")==0 || strcmp(optarg,"yes")==0 || strcmp(optarg,"true")==0 || strcmp(optarg,"True")==0) {
                    convertYes = false;
                } else {
                    throw std::invalid_argument("Invalid Argument for skipconvert");
                }
                std::cout << "Skip conversion to ascii: " << !convertYes << std::endl;
                break;
            case 'T':
                if(strcmp(optarg,"n")==0 || strcmp(optarg,"0")==0 || strcmp(optarg,"no")==0 || strcmp(optarg,"false")==0 || strcmp(optarg,"False")==0) {
                    dotrigger = true;
                } else if(strcmp(optarg,"y")==0 || strcmp(optarg,"1")==0 || strcmp(optarg,"yes")==0 || strcmp(optarg,"true")==0 || strcmp(optarg,"True")==0) {
                    dotrigger = false;
                } else {
                    throw std::invalid_argument("Invalid Argument for skiptrigger");
                }
                std::cout << "Skip trigger: " << !dotrigger << std::endl;
                break;
            case 'V':
                if(strcmp(optarg,"n")==0 || strcmp(optarg,"0")==0 || strcmp(optarg,"no")==0 || strcmp(optarg,"false")==0 || strcmp(optarg,"False")==0) {
                    visualisation = true;
                } else if(strcmp(optarg,"y")==0 || strcmp(optarg,"1")==0 || strcmp(optarg,"yes")==0 || strcmp(optarg,"true")==0 || strcmp(optarg,"True")==0) {
                    visualisation = false;
                } else {
                    throw std::invalid_argument("Invalid Argument for skipvisual");
                }
                std::cout << "Skip visualisation: " << !visualisation << std::endl;
                break;
            case 's':
                if(strcmp(optarg,"n")==0 || strcmp(optarg,"0")==0 || strcmp(optarg,"no")==0 || strcmp(optarg,"false")==0 || strcmp(optarg,"False")==0) {
                    simplePrefix = false;
                } else if(strcmp(optarg,"y")==0 || strcmp(optarg,"1")==0 || strcmp(optarg,"yes")==0 || strcmp(optarg,"true")==0 || strcmp(optarg,"True")==0) {
                    simplePrefix = true;
                } else {
                    throw std::invalid_argument("Invalid Argument for simple-prefix");
                }
                std::cout << "Use simple prefix: " << simplePrefix << std::endl;
                break;
            case 'e':
                if(strcmp(optarg,"n")==0 || strcmp(optarg,"0")==0 || strcmp(optarg,"no")==0 || strcmp(optarg,"false")==0 || strcmp(optarg,"False")==0) {
                    ecAscInput = false;
                } else if(strcmp(optarg,"y")==0 || strcmp(optarg,"1")==0 || strcmp(optarg,"yes")==0 || strcmp(optarg,"true")==0 || strcmp(optarg,"True")==0) {
                    ecAscInput = true;
                } else {
                    throw std::invalid_argument("Invalid Argument for ec-asc-input");
                }
                std::cout << "Use EC ASC input: " << ecAscInput << std::endl;
                break;
            case 't':
                thresholdsFilePathname = TString(optarg);
                std::cout << "Thresholds file: " << thresholdsFilePathname << std::endl;
                break;
            case 'A':
                minSignalsFilePathname = TString(optarg);
                std::cout << "MinSignals file: " << minSignalsFilePathname << std::endl;
                break;
            case 'B':
                maxSignalsFilePathname = TString(optarg);
                std::cout << "MaxSignals file: " << maxSignalsFilePathname << std::endl;
                break;
            case 'm':
                maskFilePathname = TString(optarg);
                std::cout << "Mask file: " << maskFilePathname << std::endl;
                break;
            case 'b':
                ave_bg_factor = std::stof(optarg);
                std::cout << "BG threshold factor: " << ave_bg_factor << std::endl;
                break;
            case 'p':
                packetSize = atoi(optarg);
                std::cout << "Packet size: " << packetSize << std::endl;
                break;
            case 'R':
                if(strcmp(optarg,"n")==0 || strcmp(optarg,"0")==0 || strcmp(optarg,"no")==0 || strcmp(optarg,"false")==0 || strcmp(optarg,"False")==0) {
                    readThresholdsForFirstPacket = false;
                } else if(strcmp(optarg,"y")==0 || strcmp(optarg,"1")==0 || strcmp(optarg,"yes")==0 || strcmp(optarg,"true")==0 || strcmp(optarg,"True")==0) {
                    readThresholdsForFirstPacket = true;
                } else {
                    throw std::invalid_argument("Invalid Argument for read thresholds for first packet");
                }
                std::cout << "Read thresholds for first packet: " << readThresholdsForFirstPacket << std::endl;
                break;
            case 'U':
                if(strcmp(optarg,"n")==0 || strcmp(optarg,"0")==0 || strcmp(optarg,"no")==0 || strcmp(optarg,"false")==0 || strcmp(optarg,"False")==0) {
                    useMinAndMaxSignals = false;
                } else if(strcmp(optarg,"y")==0 || strcmp(optarg,"1")==0 || strcmp(optarg,"yes")==0 || strcmp(optarg,"true")==0 || strcmp(optarg,"True")==0) {
                    useMinAndMaxSignals = true;
                } else {
                    throw std::invalid_argument("Invalid Argument for use minimal and maximal thresholds");
                }
                std::cout << "Use minimal and maximal signals: " << useMinAndMaxSignals << std::endl;
                break;
            case 'D':
                if(strcmp(optarg,"n")==0 || strcmp(optarg,"0")==0 || strcmp(optarg,"no")==0 || strcmp(optarg,"false")==0 || strcmp(optarg,"False")==0) {
                    useThresholdForPDM = false;
                } else if(strcmp(optarg,"y")==0 || strcmp(optarg,"1")==0 || strcmp(optarg,"yes")==0 || strcmp(optarg,"true")==0 || strcmp(optarg,"True")==0) {
                    useThresholdForPDM = true;
                } else {
                    throw std::invalid_argument("Invalid Argument for thresholds for PMT");
                }
                std::cout << "Thresholds for PMT: " << useThresholdForPDM << std::endl;
                break;
            case 'E':
                std::string::size_type sz;
                if(std::stoi(optarg,&sz) >= 0 && std::stoi(optarg,&sz) <= 32) {
                    minNumValidPMT = std::stoi (optarg,&sz);
                } else {
                    throw std::invalid_argument("Accepted-pmt could have value only between 0 and 32.");
                }
                std::cout << "Count accepted PMT: " << minNumValidPMT << std::endl;
                break;
            case 'F':
                if(strcmp(optarg,"n")==0 || strcmp(optarg,"0")==0 || strcmp(optarg,"no")==0 || strcmp(optarg,"false")==0 || strcmp(optarg,"False")==0) {
                    triggerVisual = false;
                } else if(strcmp(optarg,"y")==0 || strcmp(optarg,"1")==0 || strcmp(optarg,"yes")==0 || strcmp(optarg,"true")==0 || strcmp(optarg,"True")==0) {
                    triggerVisual = true;
                } else {
                    throw std::invalid_argument("Invalid Argument for triggerVisual for PMT");
                }
                std::cout << "Trigger visual: " << triggerVisual << std::endl;
                break;
            case 'G':
                inverseMeansFramePathname = TString(optarg);
                std::cout << "Inverse means frame file: " << inverseMeansFramePathname << std::endl;
                break;
            case 'H':
                if(strcmp(optarg,"n")==0 || strcmp(optarg,"0")==0 || strcmp(optarg,"no")==0 || strcmp(optarg,"false")==0 || strcmp(optarg,"False")==0) {
                    inverseMeansFrame = false;
                } else if(strcmp(optarg,"y")==0 || strcmp(optarg,"1")==0 || strcmp(optarg,"yes")==0 || strcmp(optarg,"true")==0 || strcmp(optarg,"True")==0) {
                    inverseMeansFrame = true;
                } else {
                    throw std::invalid_argument("Invalid Argument for coefficientMatrix");
                }
                std::cout << "Inverse means frame: " << inverseMeansFrame << std::endl;
                break;
            default:
                help = true;
                break;
        }
    }

    for (Int_t index = optind; index < argc; index++) {
        inputFiles.push_back(argv[index]);
        std::cout << "Input file: " << argv[index] << std::endl;
    }
}

Bool_t Option::isConvertYes() const {
    return convertYes;
}

const TString& Option::getDataDir() const {
    return dataDir;
}

Bool_t Option::isDotrigger() const {
    return dotrigger;
}

Bool_t Option::isEcAscInput() const {
    return ecAscInput;
}

Int_t Option::getHelp() const {
    return help;
}

const std::vector<std::string>& Option::getInputFiles() const {
    return inputFiles;
}

const TString& Option::getMaskFilePathname() const {
    return maskFilePathname;
}

const TString& Option::getMaxSignalsFilePathname() const {
    return maxSignalsFilePathname;
}

const TString& Option::getMinSignalsFilePathname() const {
    return minSignalsFilePathname;
}

Int_t Option::getPacketSize() const {
    return packetSize;
}

Bool_t Option::isSimplePrefix() const {
    return simplePrefix;
}

const TString& Option::getThresholdsFilePathname() const {
    return thresholdsFilePathname;
}

Bool_t Option::isHelp() const {
    return help;
}

Bool_t Option::isReadThresholdsForFirstPacket() const {
    return readThresholdsForFirstPacket;
}

Float_t Option::getAveBgFactor() const {
    return ave_bg_factor;
}

Bool_t Option::isUseMinAndMaxSignals() const {
    return useMinAndMaxSignals;
}

Bool_t Option::isVisualisation() const {
    return visualisation;
}

Option::~Option() {
    // TODO Auto-generated destructor stub
}

Bool_t Option::isUseThresholdForPdm() const {
    return useThresholdForPDM;
}

Int_t Option::getMinNumValidPmt() const {
    return minNumValidPMT;
}

Bool_t Option::isInverseMeansFrame() const {
    return inverseMeansFrame;
}

const TString& Option::getInverseMeansFramePathname() const {
    return inverseMeansFramePathname;
}

Bool_t Option::isTriggerVisual() const {
    return triggerVisual;
}
