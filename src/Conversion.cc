/*
 * Conversion.cc
 *
 *  Created on: Feb 18, 2018
 *      Author: Kristián Goliaš
 */

#include "Conversion.hh"

Int_t Conversion::convertFile3(TString simuout, TString outsuffix){
    //Give one root file as input as formatted by the EUSO-TA collaboration
    // root file as in 2015
    //
    //Converts from Lech to mario's format
    //PDM
    //EC from up right to bottom left
    Int_t answer = 1;

    FILE *out1,*out2;

    TString outPDM = "gzip -c > ";
    outPDM += outsuffix;
    outPDM += "_pdm_asc.gz";
    TString outEC  = "gzip -c > ";
    outEC += outsuffix;
    outEC += "_ec_asc.gz";

    out1 = popen(outPDM,"w");
    out2 = popen(outEC,"w");

    TFile  *fsimu = new TFile(simuout);

    if(!fsimu || fsimu->IsZombie()) {
        std::cerr << "File " << simuout.Data() << " does not exist. Exiting." << std::endl;
        exit(5);
        return 2;
    }

    TTree *etree = (TTree*)fsimu->Get("tevent");

    if(!etree || etree->IsZombie()) {
      std::cerr << "File " << simuout.Data() << " does not contain \"tevent\" tree. Exiting." << std::endl;
      exit(6);
      return 2;
    }

    UChar_t data[1][1][48][48];
    Int_t data_INT[48][48];
    etree->SetBranchAddress("photon_count_data",data);
    Int_t nentries_simu = (Int_t)etree->GetEntries();
    std::cout  << nentries_simu/128 << " packets to convert" << std::endl;

    for(Int_t i=0;i<nentries_simu;i++) {//runs over GTUs
        etree->GetEntry(i);
        // rotate 90 degrees counter clockwise
        for(Int_t row=0;row<48;row++) { //writes PDM-wise format (Lech)
            for(Int_t col=0;col<48;col++) {
                data_INT[row][col]=(Int_t) (data[0][0][col][47-row]);
            }
        }

        if(answer==1) {
            for(Int_t row=0;row<48;row++) {//writes PDM-wise format (Lech)
                for(Int_t col=0;col<48;col++) {
                    fprintf(out1,"%d ",data_INT[row][col]);
                }
                fprintf(out1,"\n");
            }
        }
        for(Int_t rowEC=0;rowEC<3;rowEC++) {//writes EC-wise format (Mario)
            for(Int_t colEC=0;colEC<3;colEC++) {
                for(Int_t row=0;row<16;row++) {
                    for(Int_t col=0;col<16;col++) {
                        fprintf(out2,"%d ",data_INT[rowEC*16+row][colEC*16+col]);
                    }
                    fprintf(out2,"\n");
                }
            }
        }
        if(i%(128 * 50) == 0 ) {
            std::cout << "Packet " << i/128 << " converted" << std::endl;
        }
    }

    std::cout << "Completed!" << std::endl;
    std::cout  << nentries_simu/128 << " Packets converted" << std::endl;
    fsimu->~TFile();
    fclose(out1);
    fclose(out2);

    return 0;
}

Conversion::Conversion() {
    // TODO Auto-generated constructor stub

}

Conversion::~Conversion() {
    // TODO Auto-generated destructor stub
}
